#
# $Id: Makefile 2 2014-02-04 13:34:14Z nicb $
#
TARGET_DIR=Nicola_Bernardini
TARGET=$(TARGET_DIR).zip
DEPENDENCIES=dir cv bibfiles lists papers
LISTS=$(SWLIST) $(ASLIST) $(PEDLIST) $(PUBLIST)
MKDIR=mkdir
CVDIR=../cv/en
CVSOURCE=$(CVDIR)/cv.pdf
CVTARGET=$(TARGET_DIR)/$(TARGET_DIR)_cv.pdf
CP=cp
LATEX=pdflatex
BIBTEX=bibtex
SWLIST=software.pdf
ASLIST=artistic_scientific.pdf
PEDLIST=pedagogic.pdf
PUBLIST=publications.pdf
PAPERSDIR=$(TARGET_DIR)/papers
ZIP=zip -r
RM=rm -rf

all: $(TARGET)

$(TARGET): $(DEPENDENCIES)
	$(ZIP) $@ $(TARGET_DIR)

dir: $(TARGET_DIR)

$(TARGET_DIR):
	$(MKDIR) $@

cv: $(CVSOURCE)
	$(CP) $(CVSOURCE) $(CVTARGET)

lists: $(LISTS)
	for i in $(LISTS);\
	do\
			$(CP) $$i $(TARGET_DIR)/$(TARGET_DIR)_$$i;\
	done

$(CVSOURCE):
	$(MAKE) -C $(CVDIR) -$(MAKEFLAGS)

papers: $(PAPERSDIR)
	$(CP) papers/*.pdf $(PAPERSDIR)

$(PAPERSDIR):
	$(MKDIR) $@

bibfiles: $(CVDIR)/bibliographies.tex
	$(LATEX) $(PUBLIST:.pdf=.tex)
	$(RM) $(TARGET)
	$(BIBTEX) lay
	$(BIBTEX) pro

$(PUBLIST): bibfiles

clean:
	$(RM) $(TARGET) $(TARGET_DIR) $(LISTS) *.out *.aux *.log *.bbl *.blg

.PHONY: dir cv $(CVSOURCE) papers bibfiles

.SUFFIXES: .pdf .tex

%.pdf: %.tex
	$(LATEX) $<
	$(RM) $@
	-$(BIBTEX) $*
	$(LATEX) $<
	$(RM) $@
	$(LATEX) $<
