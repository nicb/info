.so \V[CASTS]/header
.\"@(#) header.mm,v 1.11 1991/03/21 19:13:34 nicb
.\"@(#) $Id: bio.long-it.mm 1521 2013-04-09 15:27:10Z nicb $
.hy 
.SA 1
.cu
.nr Hc 2
.nr Ps 2
.ds HF 3 3 3 3 2 2 2
.ds HP +3 +2 +1
.nr Hs 4
.nr Hy 1
.\" Proprietary Markings: 1 soft, 2 hard, 3 cover, 4 author, 5 hardest,
.\"  6 CI-II enquiry
.\" .PM PM1
.\" .nr Pv 2
.\" .nr Ej 1
.de HX
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
.if \\$1=4 .SP 1
..
.de HZ
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
..
.PF "\'\'\'\s6$Revision: 1521 $ $Date: 2013-04-09 17:27:10 +0200 (Tue, 09 Apr 2013) $\s0\'"
.S +4 +4
.DS C
.B "Nicola Bernardini"
.SP 2
.S P P
.S +2 +2
.I "Nota Biografica"
.S P P
.DE
.S -3 -3
.SP 4
.ft 1
Nicola Bernardini
e` nato a Roma nel 1956.
Ha studiato composizione
con Thomas McGah e John Bavicchi
al
\f2Berklee College of Music\fP di Boston
dove si e` diplomato nel 1981.
Ha composto
lavori per strumenti elettroacustici,
elaboratore e strumenti tradizionali.
.P
In qualita` di esecutore e collaboratore tecnico
ha lavorato con musicisti quali
Claudio Ambrosini,
Giorgio Battistelli,
Luciano Berio,
Aldo Clementi,
Alvin Curran,
Adriano Guarnieri,
Kronos Quartet,
Musica Elettronica Viva (MEV),
Rova Saxophone Quartet,
Fausto Razzi,
Salvatore Sciarrino,
Marco Stroppa,
e altri.
Ha altresi` collaborato con lo scultore
Pietro Consagra e con il regista teatrale
Richard Foreman.
.P
Ha pubblicato numerosi saggi ed articoli divulgativi
su vari argomenti musicali ed in particolare
sul rapporto tra musica e nuove tecnologie.
.P
Sue composizioni recenti sono:
\f2Tre Pezzi con voce femminile\fP (1984-1987),
\f2D'Altro Canto\fP - per cinque voci femminili e live electronics (1988-1989),
\f2Ricercare IX, con quattro soggetti\fP di G. Frescobaldi
trascrizioni per orchestra (1990),
\f2Studi\fP - per pianoforte (1992-1994),
\f2Variazioni I\fP - per violoncello e quattro strumenti a fiato (1994-1996),
\f2Intermezzo-I\fP - per percussioni e flauto dolce basso (1996-1998),
\f2Recordare\fP - madrigale recitato per suoni elaborati (1999-2000).
.P
E` stato titolare della cattedra della Scuola di Musica Elettronica
del Conservatorio "C.Pollini" di Padova dal 1992 sino al 2013.
Nell'ambito dei corsi sperimentali di questo Conservatorio
ha creato e coordinato il corso di \f2Tecnico di Sala di Registrazione\fP
dal 2001 ad oggi. Per quest'ultimo corso ha progettato (in collaborazione con
i Proff.De Poli, Avanzini dell'Universita` di Padova e dell'Ing.Costa, il laboratorio \f2SaMPL\fP
(Sound and Music Processing Lab) -- primo \f2living lab\fP al mondo dedicato
alla musica e ai musicisti. Nel 2008 il laboratorio ha ricevuto un
finanziamento speciale di 600.000 Euro da parte della Fondazione Cassa di
Risparmio di Padova e Rovigo.
.P
Dal 2013 e` titolare di una delle due cattedre di Composizione Musicale
Elettroacustica del Conservatorio "S.Cecilia" di Roma.
.P
Dal 2000 al 2002 e` stato consulente del Dipartimento di Elaborazione
dell'Immagine e del Suono (VIPS) dell'Universita` di Verona
per il progetto europeo \f2SOb\fP (\f2Sounding Object\fP - IST-2000-25287)
in ambito FET (Future and Emerging Technologies).
.P
Dal 2001 al 2003 e` succeduto a Luciano Berio
alla direzione del Centro Tempo Reale
di Firenze.
In seno a questa istituzione ha coordinato le creazioni e prime esecuzioni
di numerose produzioni
di lavori musicali di grosse dimensioni quali \f2Ofani`m\fP e \f2Outis\fP
di Luciano Berio, \f2Orfeo cantando... tolse\fP, \f2Quare Tristis\fP
e \f2La Terra del Tramonto\fP di Adriano Guarnieri,
\f2Noms des Airs\fP di Salvatore Sciarrino,
\f2The Cenci\fP di Giorgio Battistelli
collaborando con istituzioni musicali quali
l'\f2Ope'ra-Bastille\fP di Parigi,
il \f2Teatro alla Scala\fP di Milano,
il \f2Theatre de la Monnaie\fP di Bruxelles,
il \f2Almeida Theatre\fP di Londra,
il \f2Teatro La Fenice\fP di Venezia,
ecc.
Ha anche creato il Dipartimento di Ricerca del Centro Tempo Reale,
con il quale ha coordinato il progetto europeo
\f2AGNULA\fP (2001-2004, IST-2001-34879, finanziamento: 1.7 MEuro)
con un consorzio costitutito da IRCAM Parigi,
Fundacio' UPF Barcelona,
Politecnico Reale di Stoccolma,
Free Software Foundation Europe
e Red Hat France.
.P
Dal 2003 al 2005 ha fondato e diretto l'unita` Media Innovation Unit,
dedicata all'innovazione tecnologica dei nuovi media,
all'interno Firenze Tecnologia,
azienda tecnologica della Camera di Commercio di Firenze.
In Media Innovation Unit ha coordinato il progetto europeo
\f2S2S^2\fP
(\f2Sound to Sense, Sense to Sound\fP, 2004-2007, IST-FET-FP6-03773, finanziamento: 1.3 MEuro)
azione di coordinamento di tutte le maggiori istituzioni di ricerca europee
sul suono per elaborare la roadmap di ricerca dei prossimi dieci anni
in questo ambito.
.P
Dal 1998 al 2001 e` stato uno dei due delegati nazionali COST
per l'azione di ricerca europea \f2G6-DAFx\fP (\f2Digital Audio Effects\fP)
con la partecipazione di 12 nazioni europee.
Dal 2003 al 2007 e` stato delegato nazionale COST-TIST e chairperson
dell'azione di ricerca europea \f2Cost287-ConGAS\fP
(\f2Gesture Control of Audio Systems\fP) che coordina le attivita`
di ricerca di 16 nazioni europee nell'ambito del gesto musicale.
Nel 2007 e` stato designato delegato nazionale dell'azione COST-TICT
IC0601-SID (Sound Interaction Design).
.P
Dall'inizio del 2006 e` il coordinatore responsabile del trasferimento
digitale della collezione di nastri del compositore Giacinto Scelsi per conto
della Fondazione Isabella Scelsi. In stretta collaborazione con l'Istituto
Centrale dei Beni Sonori e Audiovisivi/Discoteca di Stato,
questo progetto portera` al recupero
digitale e all'archiviazione di tutti i nastri prodotti da Scelsi nella
seconda parte della sua vita.
.P
Inoltre, collabora assiduamente con
il Laboratorio di Informatica Musicale del Dipartimento di Informatica e
Scienze delle Telecomunicazioni dell'Universita` di Genova ed
il Centro di Sonologia Computazionale del
Dipartimento di Elettronica ed Informatica dell'Universita` di Padova.
Nella collaborazione tra quest'ultimo e il Conservatorio di Padova e` stato
uno dei fondatori del laboratorio SaMPL (Sound and Music Processing Lab)
che ha ottenuto un finanziamento di 600 kEuro da parte della
Fondazione Cassa di Risparmio di Padova e Rovigo
per acquisizioni tecnologiche.
SaMPL e` diventato nel 2010 il primo \f2living-lab\fP al mondo
dedicato alla musica e ai musicisti.
.P
E` stato membro del Comitato Artistico di Casa Paganini a Genova,
centro di ricerca e produzione per l'informatica multimediale.
E` stato sia membro che presidente del Consiglio Direttivo dell'AIMI (Associazione di Informatica
Musicale Italiana).
E` membro e vicepresidente del CoME (Coordinamento Nazionale Docenti Musica Elettronica).
.\"
.\" $Log$
.\" Revision 1.18  2006/01/18 10:45:25  nicb
.\" reduced point size to fit in two pages
.\"
.\" Revision 1.17  2006/01/18 10:40:53  nicb
.\" minor changes
.\" updated to January 2006
.\"
.\" Revision 1.16  2005/12/16 06:50:22  nicb
.\" added technical-scientific work and updated
.\"
.\" Revision 1.15  2004/06/17 15:04:46  nicb
.\" added Media Innovation Unit
.\"
.\" Revision 1.14  2003/04/02 11:08:11  nicb
.\" updated situation at TR
.\"
.\" Revision 1.13  2002/08/28 20:06:20  nicb
.\" corrected long standing error :)
.\"
.\" Revision 1.12  2002/02/13 08:20:10  nicb
.\" corrected fonts and layout
.\"
.\" Revision 1.11  2001/02/20 15:43:44  nicb
.\" added artistic direction of Tempo Reale
.\"
.\" Revision 1.10  2000/09/23 13:05:32  nicb
.\" added subtitle to Recordare
.\"
.\" Revision 1.9  2000/05/29 03:45:49  nicb
.\" added one year to Recordare :)
.\"
.\" Revision 1.8  1999/11/25 09:02:22  nicb
.\" upgrade of the repertory and major overhaul of the header
.\"
.\" Revision 1.7  1999/08/03 17:31:54  nicb
.\" modified to suit new environment
.\"
.\" Revision 1.6  1999/07/10 19:09:33  nicb
.\" added intermezzo-I
.\"
.\" Revision 1.5  1997/10/27 21:13:03  nicb
.\" put better order to names
.\" removed reference to D'altro canto altro
.\"
.\" Revision 1.4  1997/10/06 10:39:03  nicb
.\" removed reference to Tempo Reale
.\"
.\" Revision 1.3  1996/07/23 13:26:25  nicb
.\" added piece for cello
.\"
.\" Revision 1.2  1994/11/22  13:24:05  nicb
.\" added piano piece
.\" corrected Tempo Reale passage
.\"
.\" Revision 1.1  1993/04/19  07:58:03  nicb
.\" aggiunto conservatorio di Padova
.\"
