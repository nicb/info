.so \V[CASTS]/header
.\"@(#) header.mm,v 1.11 1991/03/21 19:13:34 nicb
.\"@(#) $Id: bio.long-e.mm 1521 2013-04-09 15:27:10Z nicb $
.\" This file has been picked up from the RCS files in
.\" $HOME/me/txt/bio/bio.serious - there's more history
.\" in the RCS administration file there.
.hy 
.SA 1
.cu
.nr Hc 2
.nr Ps 2
.ds HF 3 3 3 3 2 2 2
.ds HP +3 +2 +1
.nr Hs 4
.nr Hy 1
.\" Proprietary Markings: 1 soft, 2 hard, 3 cover, 4 author, 5 hardest,
.\"  6 CI-II enquiry
.\" .PM PM1
.\" .nr Pv 2
.\" .nr Ej 1
.de HX
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
.if \\$1=4 .SP 1
..
.de HZ
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
..
.PF "\'\'\'\s6$Revision: 1521 $ $Date: 2013-04-09 17:27:10 +0200 (Tue, 09 Apr 2013) $\s0\'"
.S +4 +4
.DS C
.B "Nicola Bernardini"
.SP
.S P P
.S +2 +2
.I "Nota Biografica"
.S P P
.DE
.S -3 -3
.SP 4
.ft 1
Nicola Bernardini e` nato a Roma nel 1956. Ha studiato composizione  con
Thomas McGah e John Bavicchi al Berklee College of Music di Boston  dove
si e` diplomato  nel  1981.
Ha   composto   lavori   per   strumenti
elettroacustici, elaboratore e strumenti tradizionali.
.P
In qualita`  di  esecutore  e  collaboratore  tecnico  ha  lavorato  con
musicisti quali Claudio Ambrosini, Giorgio Battistelli,  Luciano  Berio,
Aldo Clementi, Alvin Curran, Adriano Guarnieri, Kronos  Quartet,  Musica
Elettronica Viva (MEV), Rova Saxophone Quartet, Fausto Razzi,  Salvatore
Sciarrino, Marco Stroppa,  e  altri.  Ha  altresi`  collaborato  con  lo
scultore Pietro Consagra e con il regista teatrale statunitense  Richard
Foreman.
.P
Dopo aver insegnato al Conservatorio "Cesare Pollini" di Padova per oltre 22 anni,
ha ottenuto la cattedra di Composizione Musicale Elettroacustica della Scuola
di Musica Elettronica del Conservatorio "S.Cecilia" di Roma nel 2013.
.P
Ha creato e coordinato i progetti  europei  AGNULA  (A  GNU-Linux  Audio
Distribution - IST-FP5-34789) e S2S2 (Sound to Sense, Sense  to  Sound
- IST-FP6-03774). E` stato delegato italiano delle azioni COST CostG6-
DAFx  (Digital  Audio  Effects)  e  CostIC0601-SID  (Sonic   Interaction
Design). E` stato chairperson dell'azione COST Cost287-ConGAS  (Gestural
Control of Audio Systems).
.P
Dal 2006 coordina il progetto di recupero e archiviazione digitale della
collezione di nastri del compositore Giacinto Scelsi per conto della
Fondazione Isabella Scelsi. Tale progetto e` portato avanti attraverso una
stretta collaborazione con l'Istituto Centrale dei Beni Sonori e
Audiovisivi/Discoteca di Stato.
.P
Nicola Bernardini collabora intensamente con il Laboratorio di Informatica
Musicale del Dipartimento di Informatica e Scienze delle Telecomunicazioni
dell'Universita` di Genova
e con il Centro di Sonologia Computazionale del Dipartimento d'Ingegneria
dell'Informazione dell'Universita` di Padova nella creazione di progetti e
strategie di ricerca nell'ambito del \f2Sound and Music Computing\fP.
E` stato strumentale a instaurare la collaborazione tra quest'ultimo e il
Conservatorio di Padova per creare \f2SaMPL\fP
(Sound and Music Processing Lab)
- il primo \f2living-lab\fP del mondo
interamente dedicato alla musica e ai musicisti.
\f2SaMPL\fP ha ottenuto nel 2010 un finanziamento di 600 kEuro dalla
Fondazione Cassa di Risparmio di Padova e Rovigo
dedicato all'acquisizione di tecnologie all'avanguardia.
