






                           Nicola Bernardini

                            Nota Biografica






Nicola    Bernardini  è  nato  a  Roma  nel  1956.  Ha  studiato  composizione    con
Thomas McGah e John Bavicchi al Berklee College of Music di Boston  dove
si  è  diplomato   nel   1981.  Ha   composto   lavori   per   strumenti
elettroacustici, elaboratore e strumenti tradizionali.


In qualità  di  esecutore  e  collaboratore  tecnico  ha  lavorato   con
musicisti quali Claudio Ambrosini, Giorgio Battistelli,  Luciano  Berio,
Aldo Clementi, Alvin Curran, Adriano Guarnieri, Kronos  Quartet,  Musica
Elettronica Viva (MEV), Rova Saxophone Quartet, Fausto Razzi,  Salvatore
Sciarrino, Marco Stroppa,  e  altri.  Ha  altresì  collaborato  con   lo
scultore Pietro Consagra e con il regista teatrale statunitense  Richard
Foreman.


Dopo aver insegnato al Conservatorio  "Cesare  Pollini"  di  Padova  per
oltre  22  anni,  ha ottenuto la cattedra di Composizione Musicale Elet‐
troacustica  della  Scuola  di  Musica  Elettronica  del   Conservatorio
"S.Cecilia" di Roma nel 2013.


Ha creato e coordinato i progetti  europei  AGNULA  (A  GNU‐Linux  Audio
Distribution ‐ IST‐FP5‐34789) e S2S2 (Sound to Sense, Sense  to  Sound ‐
IST‐FP6‐03774). È stato delegato italiano delle azioni COST CostG6‐ DAFx
(Digital   Audio   Effects)   e   CostIC0601‐SID   (Sonic    Interaction
Design).  È stato chairperson dell’azione COST Cost287‐ConGAS  (Gestural
Control of Audio Systems).


Dal 2006 coordina il progetto di recupero e archiviazione digitale della
collezione  di  nastri  del  compositore Giacinto Scelsi per conto della
Fondazione Isabella Scelsi. Tale progetto è  portato  avanti  attraverso
una  stretta  collaborazione  con  l’Istituto Centrale dei Beni Sonori e
Audiovisivi/Discoteca di Stato.


Nicola Bernardini collabora intensamente con il Laboratorio di Informat‐
ica Musicale del Dipartimento di Informatica e Scienze delle Telecomuni‐
cazioni dell’Università di Genova e con  il  Centro  di  Sonologia  Com‐
putazionale del Dipartimento d’Ingegneria dell’Informazione dell’Univer‐
sità di Padova nella  creazione  di  progetti  e  strategie  di  ricerca
nell’ambito  del  Sound  and  Music  Computing.   È  stato strumentale a
instaurare la collaborazione tra  quest’ultimo  e  il  Conservatorio  di
Padova per creare SaMPL (Sound and Music Processing Lab) ‐ il primo liv‐
ing‐lab del mondo interamente  dedicato  alla  musica  e  ai  musicisti.
SaMPL  ha  ottenuto  nel  2010  un finanziamento di 600 kEuro dalla Fon‐
dazione Cassa di Risparmio di Padova e Rovigo dedicato  all’acquisizione
di tecnologie all’avanguardia.









































































































































































































































 $Revision: 1521 $ $Date: 2013‐04‐09 17:27:10 +0200 (Tue, 09 Apr 2013) $


