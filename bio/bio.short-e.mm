.so \V[CASTS]/header
.\"@(#) header.mm,v 1.11 1991/03/21 19:13:34 nicb
.\"@(#) $Id: bio.long-e.mm 1521 2013-04-09 15:27:10Z nicb $
.\" This file has been picked up from the RCS files in
.\" $HOME/me/txt/bio/bio.serious - there's more history
.\" in the RCS administration file there.
.hy 
.SA 1
.cu
.nr Hc 2
.nr Ps 2
.ds HF 3 3 3 3 2 2 2
.ds HP +3 +2 +1
.nr Hs 4
.nr Hy 1
.\" Proprietary Markings: 1 soft, 2 hard, 3 cover, 4 author, 5 hardest,
.\"  6 CI-II enquiry
.\" .PM PM1
.\" .nr Pv 2
.\" .nr Ej 1
.de HX
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
.if \\$1=4 .SP 1
..
.de HZ
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
..
.PF "\'\'\'\s6$Revision: 1521 $ $Date: 2013-04-09 17:27:10 +0200 (Tue, 09 Apr 2013) $\s0\'"
.S +4 +4
.DS C
.B "Nicola Bernardini"
.SP
.S P P
.S +2 +2
.I "Biographic Notes"
.S P P
.DE
.S -3 -3
.SP 4
.ft 1
Nicola Bernardini
was born in Rome in 1956
and studied composition
with Thomas McGah and John Bavicchi at the
\f2Berklee College of Music\fP in Boston.
He has composed several works
for traditional, electroacoustic and computer instruments.
.P
After teaching at the "Cesare Pollini" Conservatory in Padova for over 22 years,
Nicola Bernardini holds a tenure Professor position
in the School of Electronic Music at the "S.Cecilia" Conservatory of Rome since 2013.
.P
From 2001 to 2003 he was appointed artistic director
of the Center Tempo Reale in Firenze
as a successor to Luciano Berio.
While there, he has coordinated the creation
and first performances of several large scale musical productions
by Luciano Berio, Adriano Guarnieri, Giorgio Battistelli, Marco Stroppa and
others.
.P
He has coordinated two large european projects
(AGNULA - 2001-2004, IST-2001-34879, funding: 1.7 MEuro,
and 
S2S^2 - Sound to  Sense,  Sense to Sound, 2004-2007, IST-FET-FP6-03773,
funding: 1.3 MEuro)
and participated to many others.
He was appointed chairman for the
COST-TIST action Cost287-ConGAS (Gesture Control of Audio Systems)
which coordinated the research activities of 16 European countries.
.P
Since 2006 he is coordinating the digital recovery and archiving of composer
Giacinto Scelsi\'s tape collection for the Fondazione Isabella Scelsi. This
project is carried out through a tight collaboration with the Discoteca di
Stato/Istituto Centrale dei Beni Sonori e Audiovisivi (the Italian National State Archive).
.P
He collaborates intensively with
the Laboratorio di Informatica Musicale of the Dipartimento di Informatica
e Scienze delle Telecomunicazioni of the University of Genova
and with
the Centro di Sonologia Computazionale of the Dipartimento
di Elettronica ed Informatica of the University of Padova
in the creation of project and research strategies
in Sound and Music Computing.
He has been instrumental in
the collaboration between the latter and the Padova Conservatory
to create SaMPL (Sound and Music Processing Lab),
the first \f2living-lab\fP in the world fully dedicated
to music and musicians.
SaMPL was awarded in 2010 a 600 kEuro funding by the
Fondazione Cassa di Risparmio di Padova e Rovigo
dedicated to technology acquisitions.
