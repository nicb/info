.so \V[CASTS]/header
.\"@(#) header.mm,v 1.11 1991/03/21 19:13:34 nicb
.\"@(#) $Id: bio.long-e.mm 1521 2013-04-09 15:27:10Z nicb $
.\" This file has been picked up from the RCS files in
.\" $HOME/me/txt/bio/bio.serious - there's more history
.\" in the RCS administration file there.
.hy 
.SA 1
.cu
.nr Hc 2
.nr Ps 2
.ds HF 3 3 3 3 2 2 2
.ds HP +3 +2 +1
.nr Hs 4
.nr Hy 1
.\" Proprietary Markings: 1 soft, 2 hard, 3 cover, 4 author, 5 hardest,
.\"  6 CI-II enquiry
.\" .PM PM1
.\" .nr Pv 2
.\" .nr Ej 1
.de HX
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
.if \\$1=4 .SP 1
..
.de HZ
.if \\$1=1 .SP 3
.if \\$1=2 .SP 3
.if \\$1=3 .SP 2
..
.PF "\'\'\'\s6$Revision: 1521 $ $Date: 2013-04-09 17:27:10 +0200 (Tue, 09 Apr 2013) $\s0\'"
.S +4 +4
.DS C
.B "Nicola Bernardini"
.SP
.S P P
.S +2 +2
.I "Biographic Notes"
.S P P
.DE
.S -3 -3
.SP 4
.ft 1
Nicola Bernardini
was born in Rome in 1956
and studied composition
with Thomas McGah and John Bavicchi at the
\f2Berklee College of Music\fP of Boston.
He has composed several works
for traditional, electroacoustic and computer instruments.
.P
As an interpreter and live-electronics performer,
Nicola Bernardini has contributed to the production
of several works by composers and ensembles such as:
Giorgio Battistelli,
Luciano Berio,
Aldo Clementi,
Alvin Curran,
Adriano Guarnieri,
Kronos Quartet,
Musica Elettronica Viva (MEV),
Rova Saxophone Quartet,
Fausto Razzi,
Salvatore Sciarrino,
Marco Stroppa,
etc.
His collaborations include works
with the artist Pietro Consagra
and the theatre director
Richard Foreman.
.P
He has published several essays and articles
on several musical and technical topics.
.P
Some of his recent compositions are:
\f2Tre Pezzi con voce femminile\fP (1984-1987),
\f2D'Altro Canto\fP - for five female voices and live-electronics (1988-1989),
\f2Ricercare IX, con quattro soggetti\fP by G. Frescobaldi
transcriptions for orchestra (1990),
\f2Studi\fP - for piano (1992-1994),
\f2Variazioni I\fP - for cello and four wind instruments (1994-1996),
\f2Intermezzo-I\fP - for percussions and bass recorder (1996-1998),
\f2Recordare\fP - madrigale recitato for processed sounds (1999-2000).
.P
Nicola Bernardini holds a tenure Professor position in
the School of Electronic Music
at the "C.Pollini" Conservatory of Padova since 1992.
He has been the coordinator of the experimental curriculum
in Sound Engineering since 2002.
.P
From 2000 to 2002 he has been a consultant for the
Vision, Image Processing and Sound
(VIPS)
Department at the Verona University for the European Project
SOb (Sounding Object - IST-2000-25287)
in the FET (Future and Emerging Technologies) context.
.P
From 2001 to 2003 he was appointed artistic director
of the Center Tempo Reale in Firenze
as a successor to Luciano Berio.
While there, he has coordinated the creation
and first performances of several large scale musical productions
such as
\f2Ofan\(`im\fP, \f2Za\(:ide\fP and \f2Outis\fP by Luciano Berio,
\f2Orfeo Cantando... Tolse\fP,
\f2Quare Tristis\fP,
\f2Pensieri Canuti\fP,
\f2La Terra del Tramonto\fP
and \f2Medea\fP by Adriano Guarnieri,
\f2Noms des Airs\fP by Salvatore Sciarrino,
\f2The Cenci\fP by Giorgio Battistelli
collaborating with musical institutions like
the Op\('era-Bastille in Paris,
the Teatro alla Scala in Milano,
the Theatre de la Monnaie in Brussels,
the Almeida Theatre in London,
the Teatro La Fenice in Venice,
etc.
He has also created the Research Department within the
Centro Tempo Reale, with which he has coordinated
the European project AGNULA (2001-2004, IST-2001-34879, funding: 1.7 MEuro)
with a European consortium constituted by
IRCAM Paris, Universitat Pompeu Fabra Barcelona,
Kungl Tekniska H\(:ogskolan Stockholm,
Free Software Foundation Europe and
Red Hat France.
.P
From 2003 to 2005 he has created and directed Media Innovation Unit (MIU)
within Firenze Tecnologia, the technological agency of the Chamber of Commerce
of Florence. This was a research unit dedicated to innovation technologies
related to new media, focusing specifically on Free Software and Free Content.
Within MIU he has coordinated the European project
S2S^2  (Sound to  Sense,  Sense to Sound, 2004-2007, IST-FET-FP6-03773,
funding: 1.3 MEuro)
a coordination action for all the largest European research centers dedicated
to sound and music computing.
.P
From 1998 to 2001 he has served as one of the two national delegates in the
COST action G6-DAFx (Digital Audio Effects) with the participation of 12
European countries. From 2003 to 2007 he has been a national delegate and chairperson
of the COST-TIST action Cost287-ConGAS (Gesture Control of Audio Systems)
which coordinates the research activities of 16 European countries.
From 2007 to 2011 he has been designated to be a national delegate of the COST-TICT action
IC0601-SID (Sound Interaction Design).
.P
Since 2006 he is coordinating the digital recovery and archiving of composer
Giacinto Scelsi\'s tape collection for the Fondazione Isabella Scelsi. This
project is carried out through a tight collaboration with the Discoteca di
Stato/Istituto dell'Audiovisivo, and  will lead to the digital recovery and archiving of all the tapes
produced by Scelsi in the second part of his life.
.P
He collaborates intensively with
the Laboratorio di Informatica Musicale of the Dipartimento di Informatica
e Scienze delle Telecomunicazioni of the University of Genova
and with
the Centro di Sonologia Computazionale of the Dipartimento
di Elettronica ed Informatica of the University of Padova
in the creation of project and research strategies
in Sound and Music Computing.
He has been instrumental in
the collaboration between the latter and the Padova Conservatory
to create SaMPL (Sound and Music Processing Lab),
the first \f2living-lab\fP in the world fully dedicated
to music and musicians.
SaMPL was awarded in 2010 a 600 kEuro funding by the
Fondazione Cassa di Risparmio di Padova e Rovigo
dedicated to technology acquisitions.
.P
He has been a member of the Artistic Committee of Casa Paganini, a Research and
Production center for Multimedia Computing in Genova.
He has also served as member and chairman of the Board of Directors of
AIMI
(Italian Computer Music Association).
.\" $Log$
.\" Revision 0.7  2006/08/08 18:50:31  nicb
.\" upgraded to the italian one
.\"
.\" Revision 0.6  2003/04/02 11:08:11  nicb
.\" updated situation at TR
.\"
.\" Revision 0.5  2001/08/15 14:36:50  nicb
.\" corrected fonting (smaller fonts)
.\"
.\" Revision 0.4  2001/08/15 11:21:47  nicb
.\" added artistic direction in Tempo Reale
.\"
.\" Revision 0.3  2000/09/23 13:05:32  nicb
.\" added subtitle to Recordare
.\"
.\" Revision 0.2  1999/08/03 17:31:54  nicb
.\" added intermezzo-I
.\"
.\" Revision 0.1  1997/10/27 21:10:35  nicb
.\" Added Kronos quartet reference; removed Berio references
.\"
.\" Revision 0.0  1996/11/21 10:10:33  nicb
.\" Initial Revision (taken from italian version 1.3 but heavily modified)
.\"
